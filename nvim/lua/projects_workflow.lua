local vim = vim

--------------------------------------------- Project listing  ------------------------------------
local list_projects = function()
  local actions = require "telescope.actions"
  local action_state = require "telescope.actions.state"

  require'telescope'.extensions.project.project{
    attach_mappings = function(prompt_bufnr, _)
      local open_project_in_current_tab = function ()
        actions.close(prompt_bufnr)
        local selection = action_state.get_selected_entry()
        vim.api.nvim_command("tcd " .. selection['path'])
      end

      local open_project_in_new_tab = function()
        actions.close(prompt_bufnr)
        local selection = action_state.get_selected_entry()
        vim.api.nvim_command("tabe")
        vim.api.nvim_command("tcd " .. selection['path'])
      end

      actions.select_default:replace(open_project_in_new_tab)
      actions.file_edit:replace(open_project_in_new_tab)
      actions.select_tab:replace(open_project_in_current_tab)
      return true
    end
}
end

--------------------------------------------- Terminal setup ------------------------------------

local keymap_opts_silent = { noremap = true, silent = true }
vim.keymap.set('n', '<leader>ps', list_projects, keymap_opts_silent)
vim.keymap.set('n', '<leader>tbs', ':lua require("telescope-tabs").list_tabs()<CR>', keymap_opts_silent)
vim.keymap.set('n', '<leader>tbp', ':lua require("telescope-tabs").go_to_previous()<CR>', keymap_opts_silent)

local Terminal = require('toggleterm.terminal').Terminal
local terminals = {}
local close_all_terminals_except = function(cwd)
  for wd,term in pairs(terminals) do
    if wd ~= cwd then -- lua doesn't have a continue statement?
      term:close()
    end
  end
end

local toggleterminal_for_tab = function()
  local cwd = vim.fn.getcwd()
  local current_tab_nr = vim.api.nvim_get_current_tabpage()
  close_all_terminals_except(cwd)
  vim.api.nvim_set_current_tabpage(current_tab_nr) -- while closing all terms, its changing tab, so bring it back
  terminals[cwd] = terminals[cwd] or Terminal:new({dir = cwd, hidden = true, direction="vertical"})
  terminals[cwd]:toggle()
end

vim.keymap.set('n', '<leader>tt', toggleterminal_for_tab, keymap_opts_silent)
vim.keymap.set('t', '<localleader>tt', function() vim.api.nvim_win_close(0, false) end, keymap_opts_silent)
vim.keymap.set('t', '<c-h>', function () vim.cmd('wincmd h') end, keymap_opts_silent)

