local vim = vim -- to avoid undefined vim warning all down the file

local keymap_opts = { noremap = true }
local keymap_opts_with_silent = { noremap = true, silent = true }
-- copy, cut and paste
vim.api.nvim_set_keymap('v', '<C-c>', '"+y', keymap_opts)

-- move through buffers
vim.api.nvim_set_keymap('n', '<leader>bN', ':bp!<CR>', keymap_opts)
vim.api.nvim_set_keymap('n', '<leader>bn', ':bn!<CR>', keymap_opts)
vim.api.nvim_set_keymap('n', '<leader>bd', ':bd!<CR>', keymap_opts)
vim.api.nvim_set_keymap('n', '<localleader>bd', ':%bd<CR>', keymap_opts)

vim.api.nvim_set_keymap('n', '<leader>ps',
  "<cmd> lua require'telescope'.extensions.project.project{display_type = 'full'}<CR>", keymap_opts)

vim.api.nvim_set_keymap('n', '<leader>u', ':UndotreeShow<CR>', keymap_opts)

vim.cmd([[
  autocmd User TelescopePreviewerLoaded setlocal wrap
]])


vim.api.nvim_set_keymap('n', '<leader>pv', ":lua require('nvim-tree.api').tree.toggle({path = vim.fn.getcwd()})<CR>", keymap_opts)
vim.api.nvim_set_keymap('n', '<localleader>pv', ":lua require('nvim-tree.api').tree.open({path = vim.fn.getcwd(), find_file = true})<CR>", keymap_opts)

-- gfiles shortcut
vim.api.nvim_set_keymap('n', '<C-p>', "<cmd>Telescope git_files<cr>", keymap_opts)
vim.api.nvim_set_keymap('n', '<Leader>pf', "<cmd>Telescope find_files<CR>", keymap_opts)
vim.api.nvim_set_keymap('n', '<leader>bf', "<cmd>Telescope buffers<cr>", keymap_opts)


-- Sweet Sweet FuGITive
vim.api.nvim_set_keymap('n', '<leader>gs', "<cmd>G<CR>", keymap_opts)

-- my own bindings or mappings"""""""""""""""""""""""""""""""""""""


vim.api.nvim_set_keymap('n', '<leader>ptag', "<cmd>execute ':Ptag '. expand('<cword>')<cr>", keymap_opts)
vim.api.nvim_set_keymap('n', '<localleader>F', "<cmd>lua require('telescope_setup').live_grep_in_glob()<cr>", keymap_opts)
vim.api.nvim_set_keymap('n', 'z=', "<cmd>lua require('telescope.builtin').spell_suggest()<cr>", keymap_opts)
vim.api.nvim_set_keymap('n', '<leader>F', "<cmd>lua require('telescope.builtin').live_grep({additional_args = {'--hidden'}})<cr>", keymap_opts)
vim.api.nvim_set_keymap('n', '<leader>ht', "<cmd>Telescope help_tags<cr>", keymap_opts)
vim.api.nvim_set_keymap('n', "<leader><Tab>", "<cmd>lua require('telescope.builtin').oldfiles({cwd_only = true})<cr>",
  keymap_opts)
vim.api.nvim_set_keymap('n', "<localleader><Tab>", "<cmd>Telescope oldfiles<cr>", keymap_opts)

vim.api.nvim_set_keymap('n', "<leader>gb", "<cmd>Telescope git_branches<cr>", keymap_opts)
vim.api.nvim_set_keymap('n', "<localleader>gl", "<cmd>Telescope git_bcommits<cr>", keymap_opts)
vim.api.nvim_set_keymap('n', "<leader>gl", "<cmd>Telescope git_commits<cr>", keymap_opts)
vim.api.nvim_set_keymap('n', "<leader>cmd", "<cmd>Telescope commands<cr>", keymap_opts)
vim.api.nvim_set_keymap('n', "<localleader>cmd", "<cmd>Telescope command_history<cr>", keymap_opts)

-- add todo
vim.api.nvim_set_keymap('n', "<leader>td", "oTODO:<C-\\><C-N>:TComment<CR>0/TODO:<CR>5la ", keymap_opts)
vim.api.nvim_set_keymap('x', "<leader>sr", "<Plug>SlimeRegionSend", keymap_opts)
vim.api.nvim_set_keymap('n', "<leader>sr", "<Plug>SlimeLineSend", keymap_opts)

-- copy current file path
vim.api.nvim_set_keymap('n', "<leader>y", "<cmd>let @+ = expand('%')<cr>", keymap_opts)
vim.api.nvim_set_keymap('n', "<leader>Y", "<cmd>let @+ = expand('%:p')<cr>", keymap_opts)
vim.api.nvim_set_keymap('n', "<leader>l", "<cmd>let @+ = expand('%') . ':' . line('.')<cr>", keymap_opts)

-- navigate quickfix lix
vim.api.nvim_set_keymap('n', "]q", "<cmd>cn<cr>", keymap_opts)
vim.api.nvim_set_keymap('n', "[q", "<cmd>cp<cr>", keymap_opts)

-- give total search count of matches
vim.api.nvim_set_keymap('n', "<leader>/", "<cmd>lua =vim.fn.searchcount({recompute=1, maxcount=10000})<cr>", keymap_opts)

-- window navigation set if not running in tmux
if(vim.fn.environ()['TERM_PROGRAM'] ~= 'tmux') then
  vim.api.nvim_set_keymap('n', "<c-j>", "<c-w>j", keymap_opts)
  vim.api.nvim_set_keymap('n', "<c-k>", "<c-w>k", keymap_opts)
  vim.api.nvim_set_keymap('n', "<c-l>", "<c-w>l", keymap_opts)
  vim.api.nvim_set_keymap('n', "<c-h>", "<c-w>h", keymap_opts)
end

-- Allow clipboard copy paste in neovide
if vim.g.neovide then
  vim.g.neovide_input_macos_option_key_is_meta = true
  vim.g.neovide_input_use_logo = 1
  vim.api.nvim_set_keymap('n', '<D-v>', '"+p', { noremap = true, silent = true })
  vim.api.nvim_set_keymap('!', '<D-v>', '<C-R>+', { noremap = true, silent = true })
  vim.api.nvim_set_keymap('t', '<D-v>', '<C-\\><C-o>"+p', { noremap = true, silent = true })
  vim.api.nvim_set_keymap('v', '<D-v>', '<C-R>+', { noremap = true, silent = true })
  vim.api.nvim_set_keymap('v', '<D-c>', '"+y', { noremap = true, silent = true })
end
