-- Uncomment below to install plug manager

local vim = vim -- to avoid undefined vim warning all down the file
-- vim.cmd [[
--  let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
--
--  if empty(glob(data_dir . '/autoload/plug.vim'))
--
--      silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
--
--      autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
--
--  endif
-- ]]

local Plug = vim.fn['plug#']


vim.call('plug#begin', '~/.config/nvim/plugged')


Plug 'christoomey/vim-tmux-navigator'

-- fugitive - git integration
Plug 'tpope/vim-fugitive'
Plug 'lewis6991/gitsigns.nvim' -- git gutter

-- tpopes
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'

Plug 'kyazdani42/nvim-web-devicons'

-- Telescope file finder / picker, two above it are dependencies
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-telescope/telescope-fzy-native.nvim'
Plug 'stevearc/dressing.nvim'

-- Project management
Plug 'nvim-telescope/telescope-project.nvim'

-- neovim language things
Plug('nvim-treesitter/nvim-treesitter', { ["do"] = ':TSUpdate' })
Plug 'neovim/nvim-lspconfig'
Plug 'jose-elias-alvarez/null-ls.nvim'
Plug 'ThePrimeagen/refactoring.nvim'
Plug 'williamboman/mason-lspconfig.nvim'
Plug 'williamboman/mason.nvim'
Plug('j-hui/fidget.nvim', { ["tag"] = 'legacy' })
-- shows and highlights signature as you type
Plug 'ray-x/lsp_signature.nvim'

Plug 'norcalli/nvim-colorizer.lua'
-- colors for brances, tags etc
Plug 'p00f/nvim-ts-rainbow'

-- Auto completion
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/nvim-cmp'
Plug 'rcarriga/cmp-dap'
Plug 'stevearc/oil.nvim'

-- Snippets
Plug 'rafamadriz/friendly-snippets'

-- For vsnip user.
Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/vim-vsnip'
-- Auto completion end

-- For scala
Plug 'scalameta/nvim-metals'
-- For android
Plug 'hsanson/vim-android'
-- Plug 'antoinemadec/FixCursorHold.nvim'
Plug 'nvim-neotest/nvim-nio'
Plug 'nvim-neotest/neotest'
Plug 'nvim-neotest/neotest-python'
Plug 'olimorris/neotest-rspec'

-- Debugger
Plug 'mfussenegger/nvim-dap'
Plug 'rcarriga/nvim-dap-ui'
Plug 'theHamsta/nvim-dap-virtual-text' -- cannot see virtual text if Treesitter for a language is not installed
Plug('Pocco81/DAPInstall.nvim', { branch = 'dev' })
Plug 'nvim-telescope/telescope-dap.nvim'
-- For ruby debugging
Plug 'suketa/nvim-dap-ruby'

-- Debugger end

-- File browser with git indicators
Plug 'kyazdani42/nvim-tree.lua'

Plug 'mbbill/undotree'

-- indent indicators and lot of mini plugins
-- Plug('echasnovski/mini.nvim', { branch = 'stable' })
Plug 'echasnovski/mini.nvim' -- use stable branch from above line once indent makes to master

-- guess the indentation of file being edited and adopt
Plug 'nmac427/guess-indent.nvim'
-- commenting out code
Plug 'numToStr/Comment.nvim'

-- auto pairs closing braces on open etc
Plug 'windwp/nvim-autopairs'

Plug 'rcarriga/nvim-notify'
-- all coloring below
-- Plug 'gruvbox-community/gruvbox'
Plug 'EdenEast/nightfox.nvim'
-- creates the status bar line showing git and other status
Plug 'nvim-lualine/lualine.nvim'
Plug 'LukasPietzschmann/telescope-tabs'


-- auto save
Plug '907th/vim-auto-save'


-- Clojure
Plug 'guns/vim-sexp'
Plug 'tpope/vim-sexp-mappings-for-regular-people'
Plug('liquidz/vim-iced', { ['for'] = 'clojure' })
Plug 'ggandor/lightspeed.nvim'

Plug 'akinsho/toggleterm.nvim'
-- Send to terminal
Plug 'jpalardy/vim-slime'
-- Lua scratchpad with eval
Plug 'rafcamlet/nvim-luapad'
Plug 'weilbith/nvim-floating-tag-preview' -- Helps preview ctags under cursor

Plug 'nvim-orgmode/orgmode'
Plug 'nagy135/typebreak.nvim'

vim.call('plug#end')

local keymap_opts = { noremap = true }
local keymap_opts_with_silent = { noremap = true, silent = true }


-- plugins configuration

-- Enable vim-iced's default key mapping
-- This is recommended for newbies
vim.g.iced_enable_default_key_mappings = true
vim.g.iced_default_key_mapping_leader = '<LocalLeader>'

-- slime config
vim.g.slime_target = "tmux"
vim.g.slime_default_config = { socket_name = "default", target_pane = "{last}" }

require('Comment').setup()
require('toggleterm').setup{
  start_in_insert = false,
  shade_terminals = false,
  size = function (term)
    if term.direction == "horizontal" then
      return 6
    elseif term.direction == "vertical" then
      return vim.o.columns * 0.4
    end
  end
}
-- let g:gruvbox_contrast_dark = 'medium'
-- if exists('+termguicolors')
--     let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
--     let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
-- endif
-- let g:gruvbox_invert_selection='0'

-- vim auto save plugin config
vim.g.auto_save = 1 -- enable AutoSave on Vim startup
vim.g.auto_save_events = { "FocusLost", "TabLeave", "WinLeave", "BufLeave" }
-- vim auto save plugin config ends

require('nightfox').setup({
  options = {
    colorblind = {
      enable = true,        -- Enable colorblind support
      simulate_only = false, -- Only show simulated colorblind colors and not diff shifted
      severity = {
        protan = 0,          -- Severity [0,1] for protan (red)
        deutan = 0.3,        -- Severity [0,1] for deutan (green)
        tritan = 0,          -- Severity [0,1] for tritan (blue)
      },
    },
  }
})
vim.cmd('colorscheme duskfox')
require 'nvim-notify-setup'
require('mini.indentscope').setup()
require 'lightspeed'.setup {
    ignore_case = true,
}
require 'orgmode_setup'
require('gitsigns_setup')
vim.cmd([[
  highlight FloatTitle guibg=none ctermbg=none
  highlight FloatBorder guibg=none guifg=grey ctermbg=none ctermfg=grey
]])
require 'dressing'.setup {
  input = {
    insert_only = false,
    win_options = {
      winblend = 0,
      winhighlight = "Normal:FloatTitle,NormalFloat:FloatTitle"
    }
  }
}
require 'telescope_setup'
require'lsp_signature'.setup()
require"fidget".setup{
  timer = {
      task_decay = 5000,
      spinner_rate = 150,
  },
  text = {
    spinner = "moon"
  }
}
require'colorizer'.setup()
require('nvim-autopairs').setup{}
require'nvim-web-devicons'.setup {}
require('lualine').setup {
  sections = {
    lualine_c = {
      {
        'filename',
        file_status = true, -- displays file status (readonly status, modified status)
        path = 1 -- 0 = just filename, 1 = relative path, 2 = absolute path
      }
    }
  },
  options = {
    globalstatus = true
  }
}
require'nvim-tree'.setup {
  update_cwd = true
}

require("nvim-treesitter.configs").setup {
  -- If TS highlights are not enabled at all, or disabled via `disable` prop, highlighting will fallback to default Vim syntax highlighting
  highlight = {
    enable = true,
    -- disable = {'org'}, -- Remove this to use TS highlighter for some of the highlights (Experimental)
    additional_vim_regex_highlighting = {'org'}, -- Required since TS highlighter doesn't support all syntax features (conceal)
  },
  ensure_installed = {'org', 'c', 'java', 'kotlin', 'python', 'javascript', 'help', 'lua', 'typescript'}, -- Or run :TSUpdate org
  rainbow = {
    enable = true,
    -- disable = { "jsx", "cpp" }, list of languages you want to disable the plugin for
    extended_mode = true, -- Also highlight non-bracket delimiters like html tags, boolean or table: lang -> boolean
    max_file_lines = nil, -- Do not enable for files with more than n lines, int
  },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = '<c-space>',
      node_incremental = '<c-space>',
    },
  },
  textobjects = {
    select = {
      enable = true,
      lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
      keymaps = {
        -- You can use the capture groups defined in textobjects.scm
        ['aa'] = '@parameter.outer',
        ['ia'] = '@parameter.inner',
        ['af'] = '@function.outer',
        ['if'] = '@function.inner',
        ['ac'] = '@class.outer',
        ['ic'] = '@class.inner',
      },
    },
    move = {
      enable = true,
      set_jumps = true, -- whether to set jumps in the jumplist
      goto_next_start = {
        [']m'] = '@function.outer',
        [']]'] = '@class.outer',
      },
      goto_next_end = {
        [']M'] = '@function.outer',
        [']['] = '@class.outer',
      },
      goto_previous_start = {
        ['[m'] = '@function.outer',
        ['[['] = '@class.outer',
      },
      goto_previous_end = {
        ['[M'] = '@function.outer',
        ['[]'] = '@class.outer',
      },
    },
  }
}
require('guess-indent').setup {}
require('neotest').setup({
  adapters = {
    require('neotest-rspec'),
    require("neotest-python")({
        args = {"--log-level", "DEBUG"},
        -- Runner to use. Will use pytest if available by default.
        -- Can be a function to return dynamic value.
        runner = "pytest",
        -- Custom python path for the runner.
        -- Can be a string or a list of strings.
        -- Can also be a function to return dynamic value.
        -- If not provided, the path will be inferred by checking for 
        -- virtual envs in the local directory and for Pipenev/Poetry configs
        python = {"pipenv", "run", "python"},
        -- python = ".venv/bin/python"
        -- Returns if a given file path is a test file.
        -- NB: This function is called a lot so don't perform any heavy tasks within it.
    })
  }
})

local fuzzy_searchable_tab = function(tab_id, buffer_ids, file_names, file_paths, is_current)
    local tab_nr = vim.api.nvim_tabpage_get_number(tab_id)
    local entry_string = vim.fn.getcwd(-1, tab_nr)
    return string.format('%d: %s%s', tab_nr, entry_string, is_current and ' <' or '')
end
require'telescope-tabs'.setup {
  entry_formatter = fuzzy_searchable_tab,
  entry_ordinal = fuzzy_searchable_tab
}

-- neotest bindings
-- runs nearest
vim.api.nvim_set_keymap('n', "<leader>tn", ":lua require('neotest').run.run()<CR>", keymap_opts_with_silent)
vim.api.nvim_set_keymap('n', "<leader>ta", ":lua require('neotest').run.attach()<CR>", keymap_opts_with_silent)
vim.api.nvim_set_keymap('n', "<leader>tf", ":lua require('neotest').run.run(vim.fn.expand('%'))<CR>",
  keymap_opts_with_silent)
vim.api.nvim_set_keymap('n', "<leader>tdn", ":lua require('neotest').run.run({strategy = 'dap'})<CR>",
  keymap_opts_with_silent)
vim.api.nvim_set_keymap('n', "<leader>ts", ":lua require('neotest').summary.open()<CR>", keymap_opts_with_silent)
vim.api.nvim_set_keymap('n', "<leader>to", ":lua require('neotest').output.open({ enter = true })<CR>",
  keymap_opts_with_silent)
vim.keymap.set('n', "<leader>tp", require('typebreak').start)

require("oil").setup()
