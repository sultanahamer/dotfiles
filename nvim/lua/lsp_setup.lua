local vim = vim -- to avoid undefined vim warning all down the file

-- lsp stuff
local keymap_opts = { noremap = true }
local keymap_opts_with_silent = { noremap = true, silent = true }

-- this setup needs to happen before lspconfig's
require("mason").setup {
  ui = {
    icons = {
      server_installed = "",
    },
  },
}
require("mason-lspconfig").setup()

local nvim_lsp = require('lspconfig')

-- Setup nvim-cmp.
local cmp = require 'cmp'

cmp.setup({
  snippet = {
    expand = function(args)
      -- For `vsnip` user.
      vim.fn["vsnip#anonymous"](args.body)
    end,
  },
  mapping = {
    ['<C-u>'] = cmp.mapping.scroll_docs(-4),
    ['<C-d>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.close(),
    ['<CR>'] = cmp.mapping.confirm({ select = false, behavior = cmp.ConfirmBehavior.Replace, }),
    ['<c-n>'] = function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      else
        fallback()
      end
    end,
    ['<c-p>'] = function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      else
        fallback()
      end
    end

  },
  sources = {
    { name = 'nvim_lsp' },
    { name = 'orgmode' },

    -- For vsnip user.
    { name = 'vsnip' },

    { name = 'buffer' },
  }
})

cmp.setup.filetype({ "dap-repl", "dapui_watches", "dapui_hover" }, {
  sources = {
    { name = "dap" },
  },
})

-- Set configuration for specific filetype.
cmp.setup.filetype('gitcommit', {
  sources = cmp.config.sources({
    { name = 'git' }, -- You can specify the `git` source if [you were installed it](https://github.com/petertriho/cmp-git).
  }, {
    { name = 'buffer' },
  })
})

local cmp_autopairs = require('nvim-autopairs.completion.cmp')
cmp.event:on('confirm_done', cmp_autopairs.on_confirm_done({ map_char = { tex = '' } }))
-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

  --Enable completion triggered by <c-x><c-o>
  buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.

  -- See `:help vim.lsp.*` for documentation on any of the below functions
  -- lsp bindings
  buf_set_keymap('n', '<leader>gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', keymap_opts_with_silent)
  buf_set_keymap('n', '<leader>gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', keymap_opts_with_silent)
  buf_set_keymap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<cr>', keymap_opts_with_silent)
  buf_set_keymap('n', '<leader>gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', keymap_opts_with_silent)
  buf_set_keymap('n', '<leader>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', keymap_opts_with_silent)
  buf_set_keymap('n', '<leader>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', keymap_opts_with_silent)
  buf_set_keymap('n', '<leader>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', keymap_opts_with_silent)
  buf_set_keymap('n', '<leader>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', keymap_opts_with_silent)
  buf_set_keymap('n', '<leader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', keymap_opts_with_silent)
  buf_set_keymap('n', '<leader><CR>', '<cmd>lua vim.lsp.buf.code_action()<CR>', keymap_opts_with_silent)
  buf_set_keymap('v', '<leader><CR>', '<cmd>lua vim.lsp.buf.range_code_action()<CR>', keymap_opts_with_silent)
  buf_set_keymap('n', '<leader>gr', "<cmd>lua require'telescope.builtin'.lsp_references()<CR>", keymap_opts_with_silent)
  buf_set_keymap('n', '<leader>e', '<cmd>lua vim.diagnostic.open_float()<cr>', keymap_opts_with_silent)
  buf_set_keymap('n', '<leader>ovr', '<cmd>Telescope lsp_document_symbols<cr>', keymap_opts_with_silent)
  buf_set_keymap('n', ']d',
    '<cmd>lua vim.diagnostic.goto_next{severity = { min = vim.diagnostic.severity.HINT, max = vim.diagnostic.severity.WARN }}<CR>',
    keymap_opts_with_silent)
  buf_set_keymap('n', '[d',
    '<cmd>lua vim.diagnostic.goto_prev{severity = { min = vim.diagnostic.severity.HINT, max = vim.diagnostic.severity.WARN }}<CR>',
    keymap_opts_with_silent)
  buf_set_keymap('n', ']e', '<cmd>lua vim.diagnostic.goto_next{severity = { min = vim.diagnostic.severity.ERROR }}<CR>',
    keymap_opts_with_silent)
  buf_set_keymap('n', '[e', '<cmd>lua vim.diagnostic.goto_prev{severity = { min = vim.diagnostic.severity.ERROR }}<CR>',
    keymap_opts_with_silent)
  buf_set_keymap('n', '<leader>q', '<cmd>lua vim.diagnostic.setloclist()<CR>', keymap_opts_with_silent)

  buf_set_keymap("n", "<leader>f", "<cmd>lua vim.lsp.buf.format()<CR>", keymap_opts_with_silent)
  buf_set_keymap("v", "<leader>f", "<cmd>lua vim.lsp.buf.format()<CR>", keymap_opts_with_silent)

  buf_set_keymap("n", "<leader>clR", "<cmd>lua vim.lsp.codelens.refresh()<cr>", keymap_opts_with_silent)
  buf_set_keymap("n", "<leader>clr", "<cmd>lua vim.lsp.codelens.run()<cr>", keymap_opts_with_silent)
end
local capabilities_with_completion = require('cmp_nvim_lsp').default_capabilities()


-- Register a handler that will be called for all installed servers.
-- Alternatively, you may also register handlers on specific server instances instead (see example below).
require("mason-lspconfig").setup_handlers { function(server_name)
  local opts = {
    on_attach = on_attach,
    flags = {
      debounce_text_changes = 150,
    },
    capabilities = capabilities_with_completion
  }
  nvim_lsp[server_name].setup(opts)
end }
--  not using local here so that its available in vim autocmd later
metals_config = require("metals").bare_config()
metals_config.on_attach = function(client, bufnr)
  on_attach(client, bufnr)
  require("metals").setup_dap()
end
vim.cmd([[autocmd FileType scala,sbt lua require("metals").initialize_or_attach(metals_config)]])

-- configure snippet expansion
vim.cmd [[
  imap <expr> <Tab>   vsnip#jumpable(1)   ? '<Plug>(vsnip-jump-next)'      : '<Tab>'
  smap <expr> <Tab>   vsnip#jumpable(1)   ? '<Plug>(vsnip-jump-next)'      : '<Tab>'
  imap <expr> <S-Tab> vsnip#jumpable(-1)  ? '<Plug>(vsnip-jump-prev)'      : '<S-Tab>'
  smap <expr> <S-Tab> vsnip#jumpable(-1)  ? '<Plug>(vsnip-jump-prev)'      : '<S-Tab>'
]]
local null_ls = require("null-ls")
null_ls.setup {
  sources = {
    null_ls.builtins.formatting.stylua,
    null_ls.builtins.diagnostics.eslint,
    null_ls.builtins.diagnostics.yamllint,
    null_ls.builtins.formatting.autopep8,
    null_ls.builtins.formatting.fixjson,

    null_ls.builtins.diagnostics.pylint,
    null_ls.builtins.code_actions.refactoring
  }
}
