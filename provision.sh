sudo apt update && sudo apt upgrade -y
sudo apt install git neovim tmux zsh curl wget ripgrep fd-find net-tools -y
sudo apt autoremove -y
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git config --global core.editor "nvim"
mkdir -p ~/.config
ssh-keygen
cat ~/.ssh/id_rsa.pub
git clone git@gitlab.com:sultanahamer/dotfiles.git ~/.config
ln -s ~/.config/.tmux.conf ~/.tmux.conf
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
